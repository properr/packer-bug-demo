Packer bug demonstration
========================

The bug was originally raised as
[Github issue #5335](https://github.com/hashicorp/packer/issues/5335), which
was closed as unable to reproduce.  It now seems most likely that we were
seeing the effects of two separate issues interacting.

The first issue related to the escaping of shell commands.  This was addressed
as part of [Github issue #5335](https://github.com/hashicorp/packer/issues/5335),
and the debugging of this part of the problem was almost certainly hindered by
[Github issue #5412](https://github.com/hashicorp/packer/issues/5412), which
remains open at the time of writing.

The second issue is that the ansible-local provisioner is not directly
compatible with the amazon-chroot builder, mainly because Ansible does not
include an "implicit localhost" as part of the "all" group.

Packer's instructions for how to use the Ansible (remote) provisioner with the
Amazon chroot builder, using Ansible's chroot connection type, do not appear to
work as written.


Environment
-----------

Affected versions: Packer 1.2.4
Host platform: Ubuntu 16.04 LTS, Jenkins slave
Builder: amazon-chroot
Provisioner: ansible-local with Ansible v2.5.4


Steps to reproduce
------------------

Set up a Jenkins build job which:
- Runs on a slave in the EC2 cloud
- Checks out the contents of this git repository
- Executes "ant build"

The build process will attempt to build AMIs with these combinations of builder
and provisioner:

1. Amazon chroot builder, Ansible local provisioner.  Observe that this fails
   with the error "provided hosts list is empty, only localhost is available",
   indicating that Ansible does not include an "implicit localhost" in the
   "all" group.

2. Amazon chroot builder, Ansible remote provisioner configured with chroot
   connection as per [the documentation](https://www.packer.io/docs/provisioners/ansible.html#chroot-communicator).
   Observe that this fails because Ansible needs to be run as root to use the
   chroot connection.

3. Amazon chroot builder, Ansible remote provisioner configured with chroot
   connection, and run under sudo.  Observe that this works as expected.

   Note the workaround with the bash script required to run ansible-playbook
   under sudo; setting the "command" option to "sudo -E ansible-playbook"
   doesn't work, although this is a separate bug.

4. Amazon chroot builder, with a file provisioner to copy the Ansible playbook
   into the chroot, a second file provisioner to copy the inventory to the
   default location /etc/ansible/hosts, and a shell (remote) provisioner to
   execute Ansible in local mode within the chroot.  Observe that this works
   as expected.

5. Amazon chroot builder, shell local provisioner used to execute Ansible
   locally with a chroot connection.  Observe that this works as expected.
