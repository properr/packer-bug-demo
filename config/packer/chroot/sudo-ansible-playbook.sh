#!/usr/bin/env bash
#
# Workaround to execute ansible-playbook under sudo
#
# @author      Steve Talbot
# @copyright   Copyright (c) 2015-18 Properr Software Ltd
# @license     MIT

sudo -E ansible-playbook "$@"
exit $?
