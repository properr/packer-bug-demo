#!/usr/bin/env bash
#
# Provisioning script run by amazon-chroot before Ansible
#
# @author      Steve Talbot
# @copyright   Copyright (c) 2015-18 Properr Software Ltd
# @license     MIT

# Stop cron jobs
service cron stop

# Try to prevent provisioner from starting services, so that volume can be umounted later...

# Prevent invoke-rc.d from starting services via an init.d script;
# Absence of shebang means this file will be executed by the shell that called it
cat <<EOF >/usr/sbin/policy-rc.d
exit 101
EOF
chmod a+x /usr/sbin/policy-rc.d

# Prevent LSB init functions from starting services
cat <<EOF >/lib/lsb/init-functions.d/00-policy-rc.d
#!/bin/sh
if test -e /usr/sbin/policy-rc.d; then
    /usr/sbin/policy-rc.d || exit $?
fi
EOF
chmod a+x /lib/lsb/init-functions.d/00-policy-rc.d

# Override systemctl, start, initctl and service commands
dpkg-divert --local --rename --add /bin/systemctl
ln -sf /bin/true /bin/systemctl
dpkg-divert --local --rename --add /sbin/start
ln -sf /bin/true /sbin/start
dpkg-divert --local --rename --add /sbin/initctl
ln -sf /bin/true /sbin/initctl
dpkg-divert --local --rename --add /usr/sbin/service
ln -sf /bin/true /usr/sbin/service

# Install Ansible dependencies
sudo apt-add-repository -y universe
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get -qq update
sudo apt-get -q -y install python2.7 python2.7-simplejson python2.7-apt python-pip
sudo /usr/bin/python2 -m pip install -U pip
sudo apt-get -q -y install software-properties-common python-software-properties
sudo apt-get -q -y install rsync aptitude
sudo apt-get -q -y install ansible=2.5.4*

# How to install Ansible when latest version in PPA is broken
#sudo pip install ansible==2.3.3.0

# Python requires /run/shm, but it's not present by default in a chroot;
# on Ubuntu, it's normally symlinked to /dev/shm, which is itself a tmpfs
mkdir -p /dev/shm
chmod 777 /dev/shm
ln -sf /dev/shm /run/shm

exit 0
