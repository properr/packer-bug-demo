#!/usr/bin/env bash
#
# Provisioning script run by amazon-chroot after Ansible
#
# @author      Steve Talbot
# @copyright   Copyright (c) 2015-18 Properr Software Ltd
# @license     MIT

# Re-enable ability to start services on server
rm -f /usr/sbin/policy-rc.d
rm -f /lib/lsb/init-functions.d/00-policy-rc.d
rm -f /bin/systemctl
dpkg-divert --local --rename --remove /bin/systemctl
rm -f /sbin/start
dpkg-divert --local --rename --remove /sbin/start
rm -f /sbin/initctl
dpkg-divert --local --rename --remove /sbin/initctl
rm -f /usr/sbin/service
dpkg-divert --local --rename --remove /usr/sbin/service

# Remove symlink so that /dev can be umounted
rm -Rf /run/shm

# Lazy-unload the bind mounts, as they may be in use by other builds;
# this means Packer won't attempt to unload them and shouldn't error
umount -l /dev/shm
umount -l /dev/pts
umount -l /dev

exit 0
